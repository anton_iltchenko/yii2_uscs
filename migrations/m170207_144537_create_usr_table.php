<?php

use yii\db\Migration;

/**
 * Handles the creation of table `usr`.
 */
class m170207_144537_create_usr_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('usr', [
            'id' => $this->primaryKey(),
            'login' => $this->string(),
            'name_first' => $this->string(),
            'name_middle' => $this->string(),
            'name_last' => $this->string(),
            'password' => $this->string(),
            'email' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('usr');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `usr_subscription`.
 * Has foreign keys to the tables:
 *
 * - `usr`
 */
class m170207_144722_create_usr_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('usr_subscription', [
            'id' => $this->primaryKey(),
            'usr_id' => $this->integer(),
            'date_end' => $this->timestamp(),
        ]);

        // creates index for column `usr_id`
        $this->createIndex(
            'idx-usr_subscription-usr_id',
            'usr_subscription',
            'usr_id'
        );

        // add foreign key for table `usr`
        $this->addForeignKey(
            'fk-usr_subscription-usr_id',
            'usr_subscription',
            'usr_id',
            'usr',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `usr`
        $this->dropForeignKey(
            'fk-usr_subscription-usr_id',
            'usr_subscription'
        );

        // drops index for column `usr_id`
        $this->dropIndex(
            'idx-usr_subscription-usr_id',
            'usr_subscription'
        );

        $this->dropTable('usr_subscription');
    }
}

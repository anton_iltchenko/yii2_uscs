<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usr".
 *
 * @property integer $id
 * @property string $login
 * @property string $name_first
 * @property string $name_middle
 * @property string $name_last
 * @property string $password
 * @property string $email
 *
 * @property UsrSubscription[] $usrSubscriptions
 */
class Usr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'name_first', 'name_middle', 'name_last', 'password', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'name_first' => 'Name First',
            'name_middle' => 'Name Middle',
            'name_last' => 'Name Last',
            'password' => 'Password',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsrSubscriptions()
    {
        return $this->hasMany(UsrSubscription::className(), ['usr_id' => 'id']);
    }
    
    public function getUsrSubscription() {
        $de = $this->UsrSubscription;
        return $de ? $de->date_end : 'nil';
    }
}

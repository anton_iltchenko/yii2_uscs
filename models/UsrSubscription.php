<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usr_subscription".
 *
 * @property integer $id
 * @property integer $usr_id
 * @property string $date_end
 *
 * @property Usr $usr
 */
class UsrSubscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usr_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usr_id'], 'integer'],
            [['date_end'], 'safe'],
            [['usr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usr::className(), 'targetAttribute' => ['usr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usr_id' => 'Usr ID',
            'date_end' => 'Date End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsr()
    {
        return $this->hasOne(Usr::className(), ['id' => 'usr_id']);
    }
}

<?php

/* @var $this yii\web\View */

$this->title = 'Users Subscriptions';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Greetings!</h1>

        <p class="lead">Welcome to User Subscription management system.</p>

        <p><a class="btn btn-lg btn-success" href="/app/web/index.php?r=usr">Proceed</a></p>
    </div>

</div>

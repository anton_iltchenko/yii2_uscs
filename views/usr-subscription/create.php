<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsrSubscription */

$this->title = 'Create Usr Subscription';
$this->params['breadcrumbs'][] = ['label' => 'Usr Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usr-subscription-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

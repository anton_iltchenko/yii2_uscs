<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsrSubscription */

$this->title = 'Update Usr Subscription: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usr Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usr-subscription-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

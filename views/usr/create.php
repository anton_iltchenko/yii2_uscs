<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usr */

$this->title = 'Create Usr';
$this->params['breadcrumbs'][] = ['label' => 'Usrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usr */

$this->title = 'Update User: ' . "{$model->name_first} {$model->name_middle} {$model->name_last}";
$this->params['breadcrumbs'][] = ['label' => 'Usrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
